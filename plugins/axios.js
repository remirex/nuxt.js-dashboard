import axios from 'axios'
export default () => {
  axios.defaults.baseURL = 'http://api.backend.test/api/v1'
}
